$("document").ready(function(){
	showDataChange();
});
var isUserLogged = false;
var isDataChangeVisible = false;
var isBusy = false;

function showDataChange(){
	$("#showPreviewPic, .showPerson").click(function(){
		if(isUserLogged){
			if(isBusy != true){
				if(isDataChangeVisible){
					isBusy = true;
					$(".myProfile").slideUp("fast", function(){
						isDataChangeVisible = false;
					});
				}else{
					isBusy = true;
					$(".myProfile").slideDown("fast", function(){
						validateInput("updateUser");
						isDataChangeVisible = true;
					});
				}
			}
		}
		isBusy = false;
	});
}

function getPreviewPic(input){
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('.myPicture').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}

function updateMe(){
	endUserValidation();
	var dontMultiply = false;
	$(".myForm").submit(function(ev){
		if(dontMultiply == false){
			if(isValid == true){
				var data = new FormData($('.myForm')[0]);
				var url = "content/editprofile/updateMe.php";
				$.ajax({
					type: "POST",
					url: url,
					processData: false,
					contentType: false,
					data: data,
					success: function(returnhtml){
						$("#outputMyMsg").fadeIn("fade", function(){
							$("#outputMyMsg").html(returnhtml);
							var msgHeight = $("#outputMyMsg").height();
							$(".myProfile").height($(".myProfile").height() + msgHeight);
							if(isItOk){
								$("#outputMyMsg").css("color", "green");
							}else{
								$("#outputMyMsg").css("color", "blue");
							}
							setTimeout(function(){
								$("#outputMyMsg").html("");
								$(".myProfile").height($(".myProfile").height() - msgHeight);
							}, 1500);
							validateInput("updateUser");
							isValid = false;
						});
					}
				});	
			}
			dontMultiply = true;
		}
		ev.preventDefault();
	});
}

function logoutMe(){
	if(confirm("Wollen Sie wirklich ausloggen?")){
		$.ajax({
			url:	"./content/login/logout.php",
			type:	"POST",
			success: function(){
				location.replace("login");
			}
		});
	}
}