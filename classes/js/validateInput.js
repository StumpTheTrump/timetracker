var email = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
/*
	Email Anforderungen:
	- Alle Zeichen
	- Ein Punkt oder Bindestrich ist erlaubt
	- Dann zwingend ein '@'-Symbol
	- Zum Schluss ein Punkt
	- Danach eine Endung bestehend aus 2-3 Zeichen
*/

var passwort = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).{4,}$/g;
/*
	Passwort Anforderungen:
	- min. 1 Grossbuchstabe
	- min. 1 Kleinbuchstabe
	- min. 1 Zeichen
	- muss grösser als 4 Zeichen sein
*/

var isUnameValid = false;
var isPwordValid = false;
var isValid = false;

//Ruft die Validierung auf
function validateInput(input){
	valUser();
	valPword();
	if(input == "updateUser"){
		isUnameValid = true;
		isPwordValid = true;
		isValid = false;
	}
}

//Validiert den Benutzernamen (die Email)
function valUser(){
	$("#usernameReg, #changeUsername").keyup(function(){
		var Name = $(this).val();
		if(Name.match(email)){
			$(this).css("borderColor", "");
			isUnameValid = true;
		}else{
			$(this).css("borderColor", "red");
			isUnameValid = false;
		}
	});
	
	$("#usernameReg, #changeUsername").change(function(){
		var Name = $(this).val();
		if(Name.match(email)){
			$(this).css("borderColor", "");
			isUnameValid = true;
		}else{
			$(this).css("borderColor", "red");
			isUnameValid = false;
		}
	});
}

//Validiert das Password
function valPword(){
	$("#passwordReg, #passwordAgainReg, #changePassword").keyup(function(){
		var Pword = $(this).val();
		if(Pword.match(passwort)){
			$(this).css("borderColor", "");
			isPwordValid = true;
		}else{
			$(this).css("borderColor", "red");
			isPwordValid = false;
		}	
	});
	
	$("#passwordReg, #passwordAgainReg, #changePassword").change(function(){
		var Pword = $(this).val();
		if(Pword.match(passwort)){
			$(this).css("borderColor", "");
			isPwordValid = true;
		}else{
			$(this).css("borderColor", "red");
			isPwordValid = false;
		}		
	});
}

//Prüft auf die Richtigkeit der Daten
function endValidation(){
	if(isUnameValid && isPwordValid){
		isValid = true;
	}else{
		isValid = false;
	}
}

//Zeigt an, ob der Benutzernamen (die Email) falsch ist,
//indem es das Feld rot umrandet
function submitValueValid(){
	var endName = $("#changeUsername").val();
	if(endName.match(email)){
		$("#changeUsername").css("borderColor", "");
		isUnameValid = true;
	}else{
		$("#changeUsername").css("borderColor", "red");
		isUnameValid = false;
	}
	
	//Zeigt an, ob das Passwort falsch ist,
	//indem es das Feld rot umrandet	
	var endPword = $("#changePassword").val();
	if(endPword != "default"){
		if(endPword.match(passwort)){
			$("#changePassword").css("borderColor", "");
			isPwordValid = true;
		}else{
			$("#changePassword").css("borderColor", "red");
			isPwordValid = false;
		}
	}else{
		$(this).css("borderColor", "");
		isPwordValid = true;
	}		
}

//Die endgültige Prüfung auf korrekte Eingabe
//Ausserdem wird hier der Endwert gesetzt
function endUserValidation(){
	submitValueValid();
	if(isUnameValid == true && isPwordValid == true){
		isValid = true;
	}else{
		isValid = false;
	}
	isUnameValid = false;
	isPwordValid = false;
}