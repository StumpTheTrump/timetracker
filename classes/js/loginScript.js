//Starte Validierung der Login und Registrierungsfunktion
$(document).ready(function(){
	validateInput();
});

var showPic = true;
var showSwitcher = true;
var isBusy = false;
var isLogedIn = false;
var isRegistred = false;

//Zeige die Profilbearbeitung an
function showProfilePic(){
	if(showPic){
		$(".picture").fadeIn("fade");
		showPic = false;
	}else{
		$(".picture").fadeOut("fade");
		showPic = true;
	}
}

//Switch zwischen der Registration und dem Login 
function switchRegister(){
	if(showSwitcher){
		if(isBusy == false){
			isBusy = true;
			$(".loginLayout").fadeOut("fade", function(){
				$(".registerLayout").fadeIn("fade", function(){
					$(".switcher").text("<< Ich kann mich bereits anmelden >>");
					isUnameValid = false;
					$("input[type=text]").val("");
					$("input[type=text]").css("borderColor", "");
					isPwordValid = false;
					$("input[type=password]").val("");
					$("input[type=password]").css("borderColor", "");
					showSwitcher = false;
					isBusy = false;
				});
			});
		}
	}else{
		if(isBusy == false){
			isBusy = true;
			$(".registerLayout").fadeOut("fade", function(){
				$(".loginLayout").fadeIn("fade", function(){
					$(".switcher").text("<< Ich muss mich erst registrieren >>");
					isUnameValid = false;
					$("input[type=text]").val("");
					$("input[type=text]").css("borderColor", "");
					isPwordValid = false;
					$("input[type=password]").val("");
					$("input[type=password]").css("borderColor", "");
					showSwitcher = true;
					isBusy = false;
				});
			});
		}
	}
}

//Versucht sich einzuloggen
//Prüft auch, ob die Daten valid sind
function submitLogin(){
	endValidation();
	var dontMultiply = false;
	$(".myLogin").submit(function(ev){
		if(dontMultiply == false){
			var data = new FormData($('.myLogin')[0]);
			var url = "./content/login/login.php";
			$.ajax({
				type: "POST",
				url: url,
				processData: false,
				contentType: false,
				data: data,
				success: function(returnhtml){
					$(".outputMsg").html(returnhtml);
					if(isLogedIn == true){
						isLogedIn = false;
						location.replace("Startseite");
					}else{
						$(".surrounderOutput").slideDown("400", function(){
							setTimeout(function(){
								$(".surrounderOutput").slideUp("3000");
							}, 2000);
						});
					}
				}
			});	
			dontMultiply = true;
		}
		ev.preventDefault();
	});
}

//Versucht sich zu registrieren
//Prüft auch, ob die Daten valid sind
function submitRegister(){
	endValidation();
	var dontMultiply = false;
	$(".myRegister").submit(function(ev){
		if(dontMultiply == false){
			dontMultiply = true;
			if(isValid){
				var data = new FormData($('.myRegister')[0]);
				var url = "./content/login/register.php";
				$.ajax({
					type: "POST",
					url: url,
					processData: false,
					contentType: false,
					data: data,
					success: function(returnhtml){
						isRegistred = false;
						$(".outputMsg").html(returnhtml);
						$(".surrounderOutput").slideDown("400", function(){
							setTimeout(function(){
								$(".surrounderOutput").slideUp("3000");
								if(isRegistred == true){
									switchRegister();
								}
							}, 2000);
						});
					}
				});
			}else{
				$(".outputMsg").html("Eingegebene Daten sind nicht valid");
				$(".surrounderOutput").slideDown("400", function(){
					setTimeout(function(){
						$(".surrounderOutput").slideUp("3000");
					}, 2000);
				});
			}
		}
		ev.preventDefault();
	});
}