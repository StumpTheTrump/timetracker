$(document).ready(function(){
	cancelNewTaskTag();
	fillSelectorWithTaskTags();
	shortcutSaveJournal();
	getPreviewJournals();
	changeSelectorMonth();
	selectCurMont();
});

function cancelNewTaskTag(auto = false){
	if(auto){
		$(".wjBlackscreenInformationDesign").hide();
		$("#wjNewTaskQuestioner").val("");
	}else{
		$(document).on("keyup", function(e){
			if(e.keyCode == 27){
				$(".wjBlackscreenInformationDesign").hide();
				$("#wjNewTaskQuestioner").val("");
			}
		});
	}
}

function shortcutSaveJournal(){
	$(window).keydown(function(event) {
		if (!(event.which == 83 && event.ctrlKey) && !(event.which == 19)) return true;
		$("#wjContextExtensionButtonSave").trigger("click", function(){
			saveNewJournal();
		});
		event.preventDefault();
		return false;
	});
}

function wjShowContextAddTag(){
	$("#wjBlackscreenInformation").show();
}

function addNewTaskTag(){
	var url = "./content/startseite/addNewTaskTag.ajax.php"
	var wjGetTag = $("#wjNewTaskQuestioner").val();
	$.ajax({
		type: "POST",
		url: url,
		data: {"wjNewTag": wjGetTag},
		success: function(returnhtml){
			$("#wjBlackscreenInformation").hide();
			$("#wjNewTaskQuestioner").val("");
			outputMessage(returnhtml);
		}
	})
}

function fillSelectorWithTaskTags(elementToReload=".wjContextTaskSelectorToFill"){
	var url = "./content/startseite/getOptionTasks.ajax.php"
	$.ajax({
		type: "POST",
		url: url,
		success: function(returnhtml){
			$(elementToReload).html(returnhtml);
		}
	})
}

function appendNewSingleTask(){
	var url = "./content/startseite/appendNewTask.ajax.php"
	wjCountSingleTasks = $('.wjContextTaskContentDesign').length;
	$.ajax({
		type: "POST",
		url: url,
		data: {"wjCountST": wjCountSingleTasks},
		success: function(returnhtml){
			$("#wjContextTaskContainer").append(returnhtml);
			fillSelectorWithTaskTags("#wjContextTaskSelector" + (wjCountSingleTasks+1));
		}
	});
}

function saveNewJournal(){
	var dontMultiply = false;
	var countAllTasks = $('.wjContextTaskContentDesign').length;
	$(".wjContextJournalFormDesign").submit(function(ev){
		if(dontMultiply == false){
			var dataform = new FormData($('.wjContextJournalFormDesign')[0]);
			dataform.append("countAllTasks", countAllTasks);
			var url = "content/startseite/saveNewJournal.ajax.php";
			$.ajax({
				type: "POST",
				url: url,
				processData: false,
				contentType: false,
				data: dataform,
				success: function(returnhtml){
					getPreviewJournals();
					outputMessage(returnhtml);
				}
			});	
			dontMultiply = true;
		}
		ev.preventDefault();
	});
}

function onResetClick(){
	var countTasks = $(".wjContextTaskContentDesign").length;
	for(var i = 2; i <= countTasks; i++){
		$("#wjContextTaskContent" + i).remove();
	}
	$(".wjOptionsJournalDesign").removeClass("wjOptionsJournalSelectedDesign");
	$("#wjContextSurrounderTitle").val("");
	$("#wjContextTaskShould1").val("");
	$("#wjContextTaskIs1").val("");
	$("#wjContextTaskComment1").val("");
	$(".wjContextTextareaDesign").val("");
}

function getPreviewJournals(){
	var url = "./content/startseite/getAllPreviewJournals.ajax.php";
	var selectedMonth = $(".wjOptionsMonthListDesign").find(":selected").text();
	$.ajax({
		type: "POST",
		url: url,
		data: {"sMonth": selectedMonth},
		success: function(returnhtml){
			$("#wjOptionsPreviewHolder").html(returnhtml);
		}
	});
}

function selectCurMont(){
	var todate = new Date();
	var Jahresmonat = todate.getMonth();
	var Monat = new Array("Januar", "Februar", "März", "April", "Mai", "Juni","Juli", "August", "September", "Oktober", "November", "Dezember");
	$("#"+Monat[Jahresmonat]).attr("selected", "selected");
}

function changeSelectorMonth(){
	$(".wjOptionsMonthListDesign").change(function(){
		clearToNewDocument();
		$("#wjOptionsPreviewHolder").html("");
		getPreviewJournals();
	});
}

function clearToNewDocument(){
	onResetClick();
}

function exportToPDF(){
	window.print();
}

function fillInRapports(rapportID, divID){
	$(".wjOptionsJournalDesign").removeClass("wjOptionsJournalSelectedDesign");
	$("#wjOptionsJournal" + divID).addClass("wjOptionsJournalSelectedDesign");
	fillInRapporttexts(rapportID, "date");
	fillInRapporttexts(rapportID, "reflex");
	fillInRapporttexts(rapportID, "prob");
	fillInRapporttexts(rapportID, "next");
	fillTasks(rapportID);
}

function fillTasks(rapID){
	var url = "./content/startseite/fillTaskInRapports.ajax.php";
	$.ajax({
		type: "POST",
		url: url,
		data: {"rapID": rapID},
		success: function(returnhtml){
			$(".wjContextTaskContentDesign").remove();
			$("#wjContextTaskContainer").append(returnhtml);
		}
	});
}

function fillInRapporttexts(rapID, getOption){
	var url = "./content/startseite/fillTextInRapports.ajax.php";
	$.ajax({
		type: "POST",
		url: url,
		data: {"rapID": rapID, "getOption": getOption},
		success: function(returnhtml){
			switch(getOption){
				case "date":
					$("#wjContextSurrounderTitle").val(returnhtml);
					break;
				case "reflex":
					$("#wjContextTextareaReflex").val(returnhtml);
					break;
				case "prob":
					$("#wjContextTextareaProblems").val(returnhtml);
					break;
				case "next":
					$("#wjContextTextareaNexttime").val(returnhtml);
					break;
				default:
					return false;
			}
		}
	});
}

function getStatistic(){
	$("#wjStatisticsSurrounder").show();
	var date = $("#wjContextSurrounderTitle").val();
	var url = "./content/startseite/getStatistic.ajax.php";
	$.ajax({
		type: "POST",
		url: url,
		data: {"date": date},
		success: function(returnhtml){
			if(returnhtml != "<b>Unter diesem Datum wurden noch keine Tasks gespeichert!</b>"){
				$(".wjStatisticsTitleDesign").html("Statistik '" + date + "'");
			}else{
				$(".wjStatisticsTitleDesign").html("Statistik");
			}
			$(".wjStatisticsTableDesign").html(returnhtml);
		}
	});
}