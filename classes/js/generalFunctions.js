//Dies sind die Allgemeinen Funktionen,
//diese können überall angewendet werden
var isMessageShow = false;

//Funktion welche eine Ausgabe von Text ermöglicht
//Wird nur eine bestimmte Zeit lang angezeigt
//Kann aber auch als Frage gestalltet werden
//-> Diese fungiert dann gleich wie das 'confirm'
function outputMessage(msg, isQuestion = false, functionIfAccept = false, functionIfDenied = false){
	if(isMessageShow == false){
		isMessageShow = true;
		if(isQuestion == false){
			$(".content > .container").prepend('<div class="surrounderOutput"><label class="outputMsg"></label></div>');
			$(".outputMsg").html(msg);
			$(".surrounderOutput").slideDown("400", function(){
				setTimeout(function(){
					$(".surrounderOutput").slideUp("3000", function(){
						$(".surrounderOutput").remove();
						isMessageShow = false;
					});
				}, 2000);
			});
		}else{	
			$(".content > .container").prepend('<div class="surrounderOutput"><label class="outputMsg">'+msg+'</label><br/><input type="button" class="msgDeniedDesign" id="msgDenied" value="Abbrechen" onclick="removeQuestionMsg(); '+functionIfDenied+'"><input type="button" class="msgAcceptDesign" id="msgAccept" value="Bestätigen" onclick="removeQuestionMsg(); '+functionIfAccept+'"></div>');
			$(".surrounderOutput").slideDown("400", function(){
				onScrollHandleButtons();
			});
		}		
	}
}

//Handelt das Aussehen der Buttons bei einer Frage
function onScrollHandleButtons(){
	if($(document).width() > "780"){
		$(".msgDeniedDesign, .msgAcceptDesign").width($(".outputMsg").width()/2);
	}
	
	$(window).resize(function() {
		if($(document).width() > "780"){
			$(".msgDeniedDesign, .msgAcceptDesign").width($(".outputMsg").width()/2);
		}else{
			$(".msgDeniedDesign, .msgAcceptDesign").width("98%");
		}
	});
}

//Entfernt das Ausgabefenster
function removeQuestionMsg(){
	$(".surrounderOutput").remove();
	isMessageShow = false;
}