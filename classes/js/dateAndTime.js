//Start des Dokuments
$( document ).ready(function() {
	getDate();
});

function getDate(){
	setInterval(function(){
		var tm = new Date();
		var hours = tm.getHours();
		var minutes = tm.getMinutes();
		var seconds = tm.getSeconds();
		
		if(hours <= 9){
			hours = "0" + hours;
		}
		
		if(minutes <= 9){
			minutes = "0" + minutes;
		}
		
		if(seconds <= 9){
			seconds = "0" + seconds;
		}
		
		var time = hours + ":" + minutes + ":" + seconds;
		$("#time").text(time);
		$("#time-mobile").text(time);
	}, 1000);
	
	var dt = new Date();
	var day = dt.getDate();
	var month = (dt.getMonth()+1)
	var year = dt.getFullYear();
	
	if(day <= 9){
		day = "0" + day;
	}
	
	if(month <= 9){
		month = "0" + month;
	}
	
	if(year <= 9){
		year = "0" + year;
	}
	
	$("#date").text(day + "." + month + "." + year);
	$("#date-mobile").text(day + "." + month + "." + year);
}