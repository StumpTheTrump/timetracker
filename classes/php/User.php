<?php 
	//Das Benutzerobjekt, damit man die Daten
	//des eingeloggten Benutzers überall zur Verfügung hat
	class User{
		public $username;
		public $userID;
		public $password;
		public $picOfMe;
		
		function __construct($selectUsername, $db) {
			$this->username = $selectUsername;
			
			$userData = uoGetUserdataStmnt($this->username, $db);
			foreach ($userData as $data) {
				$this->username = $data["email"];		
				$this->password = $data["password"];
				$this->userID = $data["userID"];
				$this->picOfMe = $data["profilePicture"];
			}
		}
	}
?>