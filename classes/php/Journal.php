<?php

// Klasse des Arbeitsjournals
class Journal{
	
	// F�gt eine neuen Arbeitsauftrag hinzu
	public function getNewSingleWorkTask($getCountedDivs){
		$getCountedDivs++;
		$newHtmlWorkTask = 
			'<div class="wjContextTaskContentDesign" id="wjContextTaskContent'.$getCountedDivs.'">
				<div class="wjContextTaskDesign" id="wjContextTaskOne">
					<select class="wjContextTaskTagDesign wjContextTaskSelectorToFill" name="wjContextTaskSelector'.$getCountedDivs.'" id="wjContextTaskSelector'.$getCountedDivs.'"></select>
				</div>
				<div class="wjContextTaskDesign" id="wjContextTaskTwo">
					<input type="time" class="wjContextTaskTagDesign" name="wjContextTaskShould'.$getCountedDivs.'" id="wjContextTaskShould'.$getCountedDivs.'" required=required>
				</div>
				<div class="wjContextTaskDesign" id="wjContextTaskThree">
					<input type="time" class="wjContextTaskTagDesign" name="wjContextTaskIs'.$getCountedDivs.'" id="wjContextTaskIs'.$getCountedDivs.'" required=required>
				</div>
				<div class="wjContextTaskDesign" id="wjContextTaskFour">
					<textarea class="wjContextTaskTagDesign" name="wjContextTaskComment'.$getCountedDivs.'" id="wjContextTaskComment'.$getCountedDivs.'"></textarea>
				</div>
			</div>';
		return $newHtmlWorkTask;
	}
	
	// Speichert / Updated das Arbeitsjournals
	public function saveJournal($POST, $db){
		$emptyItemsCounter = 0;
		if($POST["wjContextSurrounderTitle"] != ""){
			for($i = 1; $i <= $POST["countAllTasks"]; $i++){
				if($POST["wjContextTaskSelector$i"] == "" || $POST["wjContextTaskShould$i"] == "" || $POST["wjContextTaskIs$i"] == ""){
					$emptyItemsCounter++;
				}
			}
		}else{
			$emptyItemsCounter++;
		}
		if($emptyItemsCounter == 0){
			$curDate = $POST["wjContextSurrounderTitle"];
			$checkOnJournalDateGetRapID = jlSearchSpecificJournal($curDate, handleUserdata("userID"), $db);
			if($checkOnJournalDateGetRapID == null){
				jlSaveNewJournal($curDate, $POST["wjContextTextareaReflex"], $POST["wjContextTextareaProblems"], $POST["wjContextTextareaNexttime"], handleUserdata("userID"), $db);
				$getCurRapID = jlSearchSpecificJournal($curDate, handleUserdata("userID"), $db)[0]["r_id"];
				for($i = 1; $i <= $POST["countAllTasks"]; $i++){
					jlSaveNewTask($POST["wjContextTaskSelector$i"], $POST["wjContextTaskShould$i"], $POST["wjContextTaskIs$i"], $POST["wjContextTaskComment$i"], $getCurRapID, $db);
				}
				return "Das Arbeitsjournal des Tages '$curDate' wurde erstellt";
			}else{
				$checkOnJournalDateGetRapID = $checkOnJournalDateGetRapID[0]["r_id"];
				jlUpdateJournal($checkOnJournalDateGetRapID, $curDate, $POST["wjContextTextareaReflex"], $POST["wjContextTextareaProblems"], $POST["wjContextTextareaNexttime"], $db);
				jlDeleteTasks($checkOnJournalDateGetRapID, $db);
				for($i = 1; $i <= $POST["countAllTasks"]; $i++){
					jlSaveNewTask($POST["wjContextTaskSelector$i"], $POST["wjContextTaskShould$i"], $POST["wjContextTaskIs$i"], $POST["wjContextTaskComment$i"], $checkOnJournalDateGetRapID, $db);
				}
				return "Das Arbeitsjournal des Tages '$curDate' wurde gespeichert";
			}
		}else{
			return "Es wurde nicht alle Felder ausgef�llt!";
		}
	}
	
}
?>