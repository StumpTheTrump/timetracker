<?php

//Lässt es zu, dass man nur den Seitennahmen eingeben muss
//ohne dass man dabei auf einander aufbauende Seiten hat
class Contenter{

	//Ist die Variable des Kontents
	private $content;
	
	//Konstruktor
	public function __construct(){
		$this->content = array();
	}
	
	//Ruft die Überprüfung des Kontents auf
	public function getPageContent($page = null){
		if(!$page && isset($_GET['page'])){
			$page = $_GET['page'];
		}

		$page = strtolower($page);
		
		$checkContent = $this->getContent($page);
		
		if($checkContent){
			return $checkContent;
		}else{
			//Falls der Kontent nicht vorhanden ist
			//wird auf die Standardseite weitergeleitet
			return $this->getContent('Startseite');
		}
	}
	
	//Holt die Namen aller enthaltenen Ordner/Themen
	public function getEverything(){
		try{
			//DIR_CONTENT ist in der config.php Datei enthaltenen
			if(is_dir(DIR_CONTENT)){
				$folders = getFolderData(DIR_CONTENT);
				if($folders){
					foreach ($folders AS $folder){
						$this->content[$folder['filename']] = $folder;
					}
				}
			}
		}catch(Exception $e){
			die('Laden des Kontents unvollständig: ' . $e);
		}
	}
	
	//Überprüft ob es den Input/Thema als Ordner vorhanden hat
	private function getContent($topic){
		if(!$this->content){
			$this->getEverything();
		}
		
		if(isset($this->content[$topic])){
			return $this->content[$topic];
		}else{
			return false;
		}
	}

	//Falls alles korrekt vorhanden ist es keinen Fehler gab
	//wird überprüft ob es eine 'page.php' oder 'page.html' Datei im Ordner hat
	//Wenn ja wird es diese Datei so eingebunden / Ansonsten gibt es eine Fehlermeldung aus
	public function setPageContent($getPage = false){
		if($getPage != false){
			if(is_array($getPage)){
				$path = $getPage['path'];
			}else{
				$path = $this->getPageContent($getPage)['path'];
			}

			if(file_exists($path . '/page.php')){
				$data = $path . '/page.php';
			}else{
				$data = NULL;
			}
			
			if(!$data && file_exists($path . '/page.html')){
				$data = $path . '/page.html';
			}

			if($data){
				include($data);
			}else{
				echo '<h1>KEIN KONTENT GEFUNDEN!</h1>';
			}
		}else{
			echo '<h1>KEIN KONTENT GEFUNDEN!</h1>';
		}
	}
	
}