<?php
	//Hier werden die Statements in Form von Funktionen gelagert
	//Danach werden sie weitergeleitet auf eine andere Funktion,
	//welche dann die Daten auswertet und je nach Bedürfniss, einzeln oder mehrfach verarbeitet

	// USEROBJECT
	//Tag = 'uo'
	
	//Holt alle Daten zum mitgegebenen Benutzernamen
	function uoGetUserdataStmnt($uname, $db, $single = false, $toFetch = false){
		$stmt = "SELECT * FROM usertable WHERE email LIKE :email LIMIT 1";
		$params = array("email" => $uname);
		return sqlRequest($stmt, $params, $db, $single, $toFetch);
	}

	//--------------------------------------------------------------------
	// LOGIN
	//Tag = 'log'
	
	//Holt den Benutzernamen
	function logGetUsernameStmnt($uname, $db, $single = false, $toFetch = false){
		$stmt = "SELECT email FROM usertable WHERE email LIKE :email LIMIT 1";
		$params = array("email" => $uname);
		return sqlRequest($stmt, $params, $db, $single, $toFetch);
	}
	
	//Holt das Passwort
	function logGetPasswordStmnt($pword, $db, $single = false, $toFetch = false){
		$stmt = "SELECT password FROM usertable WHERE email LIKE :password LIMIT 1";
		$params = array("password" => $pword);
		return sqlRequest($stmt, $params, $db, $single, $toFetch);
	}
	
	//Holt die BenutzerID
	function logGetUserID($uname, $db, $single = false, $toFetch = false){
		$stmt = "SELECT userID FROM usertable WHERE email LIKE :email LIMIT 1";
		$params = array("email" => $uname);
		return sqlRequest($stmt, $params, $db, $single, $toFetch);
	}

	//--------------------------------------------------------------------
	// REGISTER
	//Tag = 'reg'
	
	//Holt den Benutzernamen
	function regGetUsernameStmnt($uname, $db, $single = false, $toFetch = false){
		$stmt = "SELECT email FROM usertable WHERE email LIKE :email LIMIT 1";
		$params = array("email" => $uname);
		return sqlRequest($stmt, $params, $db, $single, $toFetch);
	}
	
	//Die Daten werden gespeichert = Es wird registriert
	function regRegistrInStmnt($name, $password_safe, $photoPath, $db, $single = false, $toFetch = false){
		$stmt = "INSERT INTO usertable (email, password, profilePicture) VALUES (:name, :password_safe, :photoPath)";
		$params = array("name" => $name, "password_safe" => $password_safe, "photoPath" => $photoPath);
		return sqlRequest($stmt, $params, $db, $single, $toFetch);
	}
	
	//--------------------------------------------------------------------
	// UPDATE PROFIL
	//Tag = 'up'
	
	//Holt den Benutzernamen
	function upGetUsernameStmnt($uname, $db, $single = false, $toFetch = false){
		$stmt = "SELECT email FROM usertable WHERE email LIKE :email LIMIT 1";
		$params = array("email" => $uname);
		return sqlRequest($stmt, $params, $db, $single, $toFetch);
	}
	
	//Neuer Benutzername speichern
	function upUpdateUsernameStmnt($uname, $userID, $db, $single = false, $toFetch = false){
		$stmt = "UPDATE usertable SET email = :email WHERE userID = :ID";
		$params = array("email" => $uname, "ID" => $userID);
		return sqlRequest($stmt, $params, $db, $single, $toFetch);
	}
	
	//Neues Passwort wird gespeichert
	function upUpdatePasswordStmnt($password_safe, $userID, $db, $single = false, $toFetch = false){
		$stmt = "UPDATE usertable SET password = :safe_pw WHERE userID = :uID";
		$params = array("safe_pw" => $password_safe, "uID" => $userID);
		return sqlRequest($stmt, $params, $db, $single, $toFetch);
	}
	
	//Neues Bild wird gespeichert
	function upUpdatePictureStmnt($moveFile, $userID, $db, $single = false, $toFetch = false){
		$stmt = "UPDATE usertable SET profilePicture = :mvFile WHERE userID = :uID";
		$params = array("mvFile" => $moveFile, "uID" => $userID);
		return sqlRequest($stmt, $params, $db, $single, $toFetch);
	}
	
	//--------------------------------------------------------------------
	// WORKING JOBS
	//Tag = 'wj'
	
	//Holt alle Tags mit dem selben Namen
	function wjCheckNewTag($userID, $newTag, $db, $single = false, $toFetch = false){
		$stmt = "SELECT sw_work FROM singlework JOIN usertable ON userID = fk_swUserID WHERE userID = :uID AND sw_work = :newT";
		$params = array("uID" => $userID, "newT" => $newTag);
		return sqlRequest($stmt, $params, $db, $single, $toFetch);
	}
	
	//Fügt einen neuen Arbeitstag hinzu
	function wjAddNewTag($newTag, $userID, $db, $single = false, $toFetch = false){
		$stmt = "INSERT INTO singlework (sw_work, fk_swUserID) VALUES (:work, :uID)";
		$params = array("work" => $newTag, "uID" => $userID);
		return sqlRequest($stmt, $params, $db, $single, $toFetch);
	}
	
	//Holt alle Tasks eines Benutzers
	function wjGetTaskFromUser($userID, $db, $single = false, $toFetch = false){
		$stmt = "SELECT sw_work FROM singlework JOIN usertable ON userID = fk_swUserID WHERE userID = :uID";
		$params = array("uID" => $userID);
		return sqlRequest($stmt, $params, $db, $single, $toFetch);
	}
	
	//--------------------------------------------------------------------
	// JOURNAL
	//Tag = 'JL'
	
	//Sucht unter diesem Datum ein Journal
	function jlSearchSpecificJournal($dati, $userID, $db, $single = false, $toFetch = false){
		$stmt = "SELECT r_id FROM rapports as r JOIN usertable as u ON u.userID LIKE r.fk_userID WHERE u.userID LIKE :uID AND r.r_date LIKE :date LIMIT 1";
		$params = array("uID" => $userID, "date" => $dati);
		return sqlRequest($stmt, $params, $db, $single, $toFetch);
	}
	
	//Speichert ein neues Journal ab
	function jlSaveNewJournal($date, $reflex, $probs, $nextmove, $userID, $db, $single = false, $toFetch = false){
		$stmt = "INSERT INTO rapports (r_date, r_reflexion, r_problems, r_nexttext, fk_userID) VALUES (:date, :refle, :probs, :next, :uID)";
		$params = array("date" => $date, "refle" => $reflex, "probs" => $probs, "next" => $nextmove, "uID" => $userID);
		return sqlRequest($stmt, $params, $db, $single, $toFetch);
	}
	
	//Speichere einen neuen Task
	function jlSaveNewTask($tname, $tstime, $titime, $tdescr, $tRapID, $db, $single = false, $toFetch = false){
		$stmt = "INSERT INTO usertasks (t_taskname, t_shouldtime, t_istime, t_description, fk_rapportID) VALUES (:name, :stime, :itime, :desc, :rID)";
		$params = array("name" => $tname, "stime" => $tstime, "itime" => $titime, "desc" => $tdescr, "rID" => $tRapID);
		return sqlRequest($stmt, $params, $db, $single, $toFetch);
	}
	
	//Updated ein vorhandenes Arbeitsjournal
	function jlUpdateJournal($rapID, $date, $reflex, $probs, $next, $db, $single = false, $toFetch = false){
		$stmt = "UPDATE rapports SET r_date = :dati, r_reflexion = :refli, r_problems = :probis, r_nexttext = :nexti WHERE r_id = :rID";
		$params = array("dati" => $date, "refli" => $reflex, "probis" => $probs, "nexti" => $next, "rID" => $rapID);
		return sqlRequest($stmt, $params, $db, $single, $toFetch);
	}
	
	//Lösche vorhandene Tasks (Intention->Update)
	function jlDeleteTasks($rapID, $db, $single = false, $toFetch = false){
		$stmt = "DELETE FROM usertasks WHERE fk_rapportID = :rID";
		$params = array("rID" => $rapID);
		return sqlRequest($stmt, $params, $db, $single, $toFetch);
	}
	
	//Holt alle Arbeitsjournale unter einem bestimmten Monat heraus
	function jlDGetAllPreviewJournals($month, $uID, $db, $single = false, $toFetch = false){
		$stmt = "SELECT r_id, r_date FROM rapports as r JOIN usertable as u ON u.userID LIKE r.fk_userID WHERE u.userID LIKE :uID AND r.r_date LIKE :date ORDER BY r_date DESC";
		$params = array("uID" => $uID, "date" => $month);
		return sqlRequest($stmt, $params, $db, $single, $toFetch);
	}
	
	//Holt alle Tasks unter einer bestimmten ID heraus
	function jlGetTaskFromID($rapID, $db, $single = false, $toFetch = false){
		$stmt = "SELECT * FROM usertasks WHERE fk_rapportID = :rID";
		$params = array("rID" => $rapID);
		return sqlRequest($stmt, $params, $db, $single, $toFetch);
	}
	
	//Holt alle Tags, ausgenommen eines bestimmten Tagnames
	function jlGetEmptyTasks($userID, $tagToExclude, $db, $single = false, $toFetch = false){
		$stmt = "SELECT sw_work FROM singlework WHERE fk_swUserID = :uID and sw_work != :toExclude";
		$params = array("uID" => $userID, "toExclude" => $tagToExclude);
		return sqlRequest($stmt, $params, $db, $single, $toFetch);
	}
	
	//Holt das Datum eines Rapports
	function jlGetDateFromRapport($rID, $db, $single = false, $toFetch = false){
		$stmt = "SELECT r_date FROM rapports WHERE r_id = :rID";
		$params = array("rID" => $rID);
		return sqlRequest($stmt, $params, $db, $single, $toFetch);
	}
	
	//Holt die Reflexion eines Rapports
	function jlGetReflexFromRapport($rID, $db, $single = false, $toFetch = false){
		$stmt = "SELECT r_reflexion FROM rapports WHERE r_id = :rID";
		$params = array("rID" => $rID);
		return sqlRequest($stmt, $params, $db, $single, $toFetch);
	}
	
	//Holt das Problem eines Rapports
	function jlGetProbFromRapport($rID, $db, $single = false, $toFetch = false){
		$stmt = "SELECT r_problems FROM rapports WHERE r_id = :rID";
		$params = array("rID" => $rID);
		return sqlRequest($stmt, $params, $db, $single, $toFetch);
	}
	
	//Holt das weitere Vorgehen eines Rapports
	function jlGetNextFromRapport($rID, $db, $single = false, $toFetch = false){
		$stmt = "SELECT r_nexttext FROM rapports WHERE r_id = :rID";
		$params = array("rID" => $rID);
		return sqlRequest($stmt, $params, $db, $single, $toFetch);
	}
	
	//Überprüft ob unter diesem Datum ein Rapport für den Benutzer verfügbar ist
	function jlCheckDateFromUser($date, $uID, $db, $single = false, $toFetch = false){
		$stmt = "SELECT r_id FROM rapports WHERE fk_userID = :uID and r_date = :date";
		$params = array("uID" => $uID, "date" => $date);
		return sqlRequest($stmt, $params, $db, $single, $toFetch);
	}
?>