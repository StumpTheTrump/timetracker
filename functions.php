<?php
// Falls man eingeloggt ist, kann man nicht auf die Loginseite
// Falls man nicht eingeloggt ist, kann man nur auf die Loginseite
function handleSessionLogin(){
	if(isset($_SESSION["userdata"])){
		$path = explode("/", $_SERVER['REQUEST_URI']);
		$lastElement = end($path);
		if(strtolower($lastElement) == "login"){
			header("location: Startseite");
			exit;
		}
	}else{
		$path = explode("/", $_SERVER['REQUEST_URI']);
		$lastElement = end($path);
		if(strtolower($lastElement) != "login"){
			header("location: Login");
			exit;
		}
	}
}

// Überprüft ob man auch wirklich eingeloggt ist
// und somit die Berechtigung hat, die Ajaxfunktion auszuführen
// Oder holt Daten vom Userobjekt
function handleUserdata($returnvalue = false){
	if(!isset($_SESSION)){
		session_start();
	}
	if(isset($_SESSION["userdata"])){
		$benutzerdata = unserialize($_SESSION["userdata"]);
		switch(strtolower($returnvalue)){
			case "username":
				return $benutzerdata->username;
				break;
			case "userid":
				return $benutzerdata->userID;
				break;
			case "picofme":
				return $benutzerdata->picOfMe;
				break;
			default:
				return true;
				break;
		}
	}else{
		return false;
	}
}

//Fragt ab, ob der Benutzer auf die Bilder zugreiffen möchte
function deniePermissionToPics($checkUrl){
	if(stristr($checkUrl, "/img") === TRUE){
		return $checkUrl;
	}
}

//Holt aus einer URL den passenden GET Parameter und gibt diesen zurück
function getGetData($url, $returnvalue){
	$debrisSacrifices = explode($returnvalue . "=", $url);
	return end($debrisSacrifices);
}

// Handelt alle SQL Anfragen
//(Es wurde alles mit Prepared Statements (PDOs) erstellt,
//um die Sicherheit gewährleisten zu können)
function sqlRequest($sqlStatement, $parameter, $db, $single = false, $toFetch = false){
	$finStmt = $db->prepare($sqlStatement);
	$finStmt->execute($parameter);
	if($single == false){
		return $finStmt->fetchAll(PDO::FETCH_ASSOC);
	}else{
		$result = $finStmt->fetch(PDO::FETCH_OBJ);
		return $result->$toFetch;
	}
}

//Sichere Art bei der Ausgabe von Text
//Um Cross-Site-Scripting abzuhalten
function secureOutput($parameter){
	return htmlspecialchars($parameter, ENT_QUOTES, 'UTF-8');
}

//Holt die Informationen von dem Ordnerpfad welcher mitgegeben wird
function getFolderData($path, $extension = null){
	//Holt alle Dateien aus dem Pfad
	if(is_dir($path)){
		$dir = array_diff( scandir($path, 1), array('..', '.') );
		if($dir){
			$files = array();
			
			//Nimmt jedes einzelne File
			foreach($dir AS $file){
				
				//Bekommt nur den Dateinamen
				$filename = explode(".", $file);
				//Bekommt nur den Anhang
				$fileextention = $filename[count($filename) - 1];

				//Speichert den Dateinamen, Pfad und Anhang in einem Array
				if( ($extension && $extension == $fileextention) || !$extension ){
					$tmp = array();
					$tmp['filename'] = $file;
					$tmp['extention'] = $fileextention;
					$tmp['path'] = $path . $file;
					array_push($files, $tmp);
				}
			}

			if(count($files) > 0){
				return $files;
			}
		}
	}
	//Falls keine gefunden
	return false;
}

//Automatisches Einfügen der JS Dateien
function includeJS(){
	try{
		//get files
		$files = getFolderData(DIR_JS, 'js');
		if($files){
			foreach($files AS $file){
				//include file tags
				echo '<script type="text/javascript" src="' . $file['path'] . '"></script>';
			}
		}
	}catch(Exception $e){
		die('Unable to implement JS-Classes: ' . $e);
	}
}

//Automatisches Einfügen der PHP Dateien
function includePHP(){
	try{
		//Alle Dateien nehmen
		$files = getFolderData(DIR_PHP, 'php');
		if($files){
			foreach($files AS $file){
				//Alle Dateien einfügen
				include_once($file['path']);
			}
		}
	}catch(Exception $e){
		die('Folgende Datei konnte nicht eingefügt werden: ' . $e);
	}
}

//Automatisches Einfügen der CSS Dateien
function includeCSS(){
	try{
		//Alle Dateien nehmen
		$files = getFolderData(DIR_CSS, 'css');
		if($files){
			foreach($files AS $file){
				echo '<link rel="stylesheet" type="text/css" href="' . $file['path'] . '" />';
			}
		}
	}catch(Exception $e){
		die('Unable to implement CSS: ' . $e);
	}
}

//Generiert aus allen Angaben aus der Config, das PDO Element (Für die Pages gedacht -> durch automatischen Include);
function getDB($host, $name, $pw, $database){
	return new PDO("mysql:host=".$host.";dbname=".$database."", $name, $pw);
}
?>