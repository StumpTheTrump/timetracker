<?php
	//Das Configfile wird geladen
	include_once('config.php');
	
	//Alle PHP Dateien werden eingefügt
	includePHP();
	
	//Der gesamte Textkontent wird auf UTF-8 Standard gesetzt
	header('Content-Type: text/html; charset=UTF-8');
	
	//Initialisiert einen neuen Contenter
	$content = new Contenter();
?>

<!-- Grundstruktur -->

<!DOCTYPE html>
<html lang="de">

<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=yes, width=device-width" />
	<meta name="description" content="CONTENT">
    <meta name="author" content="Nico Bieri">

	<script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
	
	<title>TimeTracker - Try TimeTracker to track the time</title>

	<?php
		//Alle CSS Dateien werden eingefügt
		includeCSS();
	
		//Alle Javascript Dateien werden eingefügt
		includeJS();
		
		//Die Statements werden eingefügt
		include_once(DIR_SQL . "statements.php");
		
		//Prüft auf die Berechtigung eines Benutzercontrolls
		session_start();
		handleSessionLogin();
		if(handleUserdata()){
			$username = handleUserdata("username");
			$picOfMe = handleUserdata("picOfMe");
			$picOfMe = str_replace("../../", "./",  $picOfMe);
			
			if(!file_exists($picOfMe)){
				$picOfMe = "./img/userpics/default.png";										
			}
		}else{
			$username = "";
			$picOfMe = "./img/userpics/default.png";	
		}
	?>
	
	
</head>

<body>
	<div class="backgroundWhiteFlaireDesign"></div>
	<div class="backgroundImagerDesign"></div>
	<div id="page-wrapper">
		<div class="surrounder">
		
			<header>
				<div class="container">
					<label class="headerTitle">TimeTracker</label>
					<div class="holderShowPic">
						<img src='<?php echo $picOfMe ?>' id='showPreviewPic' name='showPreviewPic'/>
					</div>
					<label class="showPerson"><?php echo $username ?></label>
					<?php
					//Profil ändern wird eingefügt
					if(handleUserdata()){
						include_once(DIR_CONTENT . "editprofile/page.php");
					}
					?>
				</div>
			</header>
			
			<div class="content">
				<div class="container">
					
					<?php
						//Der Kontent wird eingefügt
						$page = $content->getPageContent();
						if($page['filename']){
							$content->setPageContent($page);
						}
					?>
					
				</div>
			</div>
		</div>
	</div>
	
	<footer>
		<div class="default-footer">
			<p>
				<label id="author">NB, MM, JD</label>
				<label id="date" class="designFooterlabel"></label>
				<label id="time" class="designFooterlabel"></label>
			</p>
		</div>
		
		<div class="mobile-footer">
			<p>
				<label id="author">NB, MM, JD</label>
				<label id="date-mobile" class="designFooterlabel"></label>
				<label id="time-mobile" class="designFooterlabel"></label>
			</p>
		</div>
	</footer>
</body>
</html>