<?php
	//Es muss das configfile für das Ajax included werden
	require_once("../../config.ajax.php");

	//Mittels einer Funktion holt es die Daten vom Benutzer aus dem Benutzerobjekt
	$userID = handleUserdata("userID");
	$profilePicDB = handleUserdata("picOfMe");
	
	//Die POST-Variablen werden geholt
	$curUsername = strtolower($_POST["changeUsername"]);
	$curPassword = $_POST["changePassword"];
	
	//Ein Array wird erstellt für die Errors
	$error = array();
	
	//Prüft auf die gesetzten Variablen
	if((isset($curUsername) == true) && (isset($curPassword) == true) && (handleUserdata() == true)){
		//Holt den Benutzernamen
		$selectUsername = upGetUsernameStmnt($curUsername, $db);
		if($selectUsername){
			$selectUsername = upGetUsernameStmnt($curUsername, $db, true, 'email');
		}
		
		//Updated den Benutzername 
		if($selectUsername != $curUsername){
			if(preg_match('/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/', $curUsername)){
				$updateUsername = upUpdateUsernameStmnt($curUsername, $userID, $db);
			}else{
				$error[] = 'Der Benutzername erfüllt nicht das richtige Format!';
			}
		}

		//Updated das Passwort
		if($curPassword != "default"){
			if(preg_match('/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).{4,}$/', $curPassword)){
				$secret_salt = "setasalt";
				$salted_password = $secret_salt . $curPassword;
				$password_safe = hash('sha256', $salted_password);
				upUpdatePasswordStmnt($password_safe, $userID, $db);
			}else{
				$error[] = 'Das Passwort erfüllt nicht das richtige Format!';
			}
		}
		
		//Speichert das neue Bild in der DB und auf im Ordner
		if(isset($_FILES['takePic'])) {
			if($_FILES['takePic']['error'] != UPLOAD_ERR_NO_FILE) {
				
				$maxsize = 1048576;
							
				$acceptable = array(
					'image/jpeg',
					'image/JPG',
					'image/jpg',
					'image/gif',
					'image/png'
				);

				if(!in_array($_FILES['takePic']['type'], $acceptable)){
					$error[] = 'Unerlaubter Dateityp! ';
				}
				
				if(($_FILES['takePic']['size'] >= $maxsize) || ($_FILES["takePic"]["size"] == 0)) {
					$error[] = ' File ist grösser als 1MB';
				}

				if(count($error) === 0) {
					//Falls es kein Standardbild ist, lösche es
					if(file_exists($profilePicDB)){
						if($profilePicDB !== "./img/userpics/default.png"){
							unlink($profilePicDB);
						}
					}
					
					//Neuer Bildname erstellen
					$newName = "user-" . $userID . "-" . uniqid() . ".jpg";
					$moveFile = "../../img/userpics/";
					$moveFile .= $newName;
					$showFile = "./img/userpics/" . $newName;
					
					//Verschiebe das Bild
					move_uploaded_file($_FILES['takePic']['tmp_name'], $moveFile);
					
					//Setzt Bildlink in die Datenbank
					upUpdatePictureStmnt($moveFile, $userID, $db);
				}
			}
		}
		
		//Falls es Errors hat, gib diese aus
		//Ansonsten Update alles
		if(count($error) == 0){
			$_SESSION["userdata"] = serialize(new User($curUsername, $db));
			echo "<script>
					isItOk = true;
					$('.showPerson').html('$curUsername');
				</script>";
			if($_FILES['takePic']['error'] != UPLOAD_ERR_NO_FILE){
				echo "<script>$('#showPreviewPic').attr('src', '$showFile');</script>";
			}
			echo secureOutput($curUsername." gespeichert");
		}else{
			echo "<script>isItOk = false;</script>";
			for($i = 0; $i<= count($error)-1; $i++){
				echo secureOutput($error[$i])."</br>";
			};
		}	
	}else{
		echo "<script>isItOk = false;</script>";
		echo secureOutput("Leere Eingabe");
	}
?>