<!--
-------------------------------
Die Seite ist nur zum einlesen
und wird nur vom php include
-------------------------------
Ist für die Änderungen am eigenen Benutzerprofil zuständig
-->

<script>isUserLogged = true;</script>

<div class="myProfile">
	<label id="outputMyMsg"></label>
	<div class="picHolder">
		<img src="<?php echo secureOutput($picOfMe) ?>" class="myPicture" />
	</div>
	<form method="POST" action="" class="myForm" enctype="multipart/form-data">
		<input type="file" class="takePic" name="takePic" onchange="getPreviewPic(this)" />
		<label class="designTitle">Benutzername</label>
		<input type="text" class="changeData" value="<?php echo secureOutput($username) ?>" id="changeUsername" name="changeUsername"/>
		<label class="designTitle">Password</label>
		<input type="password" class="changeData" value="default" id="changePassword" name="changePassword"/></br>
		<div class="buttonHolder">
			<input type="button" class="logoutMe" onclick="logoutMe()" value="Logout" />
			<input type="submit" class="submitNewMe" onclick="updateMe()" />
		</div>
	</form>
</div>