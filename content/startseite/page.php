<!-- Content zur Startseite -->
<!-- Tag = wj -->

<div class="wjSiteSurrounderDesign" id="wjSiteSurrounder">
	<div class="wjOptionsSurrounderDesign" id="wjOptionsSurrounder">
		<div class="wjOptionsSurrounderFilterDesign" id="wjOptionsSurrounderFilter">
			<select class="wjOptionsMonthListDesign" name="monthList" type="text">
				<option id="Januar" value="januar">Januar</option>
				<option id="Fabruar" value="Fabruar">Fabruar</option>
				<option id="März" value="März">März</option>
				<option id="Aprli" value="Aprli">Aprli</option>
				<option id="Mai" value="Mai">Mai</option>
				<option id="Juni" value="Juni">Juni</option>
				<option id="Juli" value="Juli">Juli</option>
				<option id="August" value="August">August</option>
				<option id="September" value="September">September</option>
				<option id="Oktober" value="Oktober">Oktober</option>
				<option id="November" value="November">November</option>
				<option id="Dezember" value="Dezember">Dezember</option>
			</select>
		</div>
		<div class="wjOptionsSurrounderContentDesign" id="wjOptionsSurrounderContent">
			<div class="wjOptionsJournalDesign" id="wjOptionsJournalAddNew" onclick="clearToNewDocument()">+</div>
			<div class="wjOptionsPreviewHolderDesign" id="wjOptionsPreviewHolder"></div>
		</div>
	</div>

	<div class="wjContextSurrounderDesign" id="wjContextSurrounder">
		<form class="wjContextJournalFormDesign" id="wjContextJournalForm">
			<div class="jwContextDateTitleSurrounderDesign">
				<input type="date" class="wjContextSurrounderTitleDesign" id="wjContextSurrounderTitle" name="wjContextSurrounderTitle" required=required>
			</div>
			<div class="wjContextHolderDesign">
				<div class="wjContextSurrounderTasksDesign" id="wjContextSurrounderTasks">
					<div class="wjContextTaskContainerDesign" id="wjContextTaskContainer">
						<div class="wjContextTaskTitleSurrounderDesign" id="wjContextTaskTitleSurrounder">
							<div class="wjContextTaskTitleDesign" id="wjContextTaskTitleOne">Aufgabe</div>
							<div class="wjContextTaskTitleDesign" id="wjContextTaskTitleTwo">Soll-/ Zeit</div>
							<div class="wjContextTaskTitleDesign" id="wjContextTaskTitleThree">Ist-/ Zeit</div>
							<div class="wjContextTaskTitleDesign" id="wjContextTaskTitleFour">Bemerkung</div>
						</div>
						<div class="wjContextTaskContentDesign" id="wjContextTaskContent1">
							<div class="wjContextTaskDesign" id="wjContextTaskOne">
								<select class="wjContextTaskTagDesign wjContextTaskSelectorToFill" name="wjContextTaskSelector1" id="wjContextTaskSelector1"></select>
							</div>
							<div class="wjContextTaskDesign" id="wjContextTaskTwo">
								<input type="time" class="wjContextTaskTagDesign" name="wjContextTaskShould1" id="wjContextTaskShould1" required=required>
							</div>
							<div class="wjContextTaskDesign" id="wjContextTaskThree">
								<input type="time" class="wjContextTaskTagDesign" name="wjContextTaskIs1" id="wjContextTaskIs1" required=required>
							</div>
							<div class="wjContextTaskDesign" id="wjContextTaskFour">
								<textarea class="wjContextTaskTagDesign" name="wjContextTaskComment1" id="wjContextTaskComment1"></textarea>
							</div>
						</div>
					</div>
					<div class="wjContextTaskOptionsSurrounderDesign" id="wjContextTaskOptionsSurrounder">
						<div class="wjContextTaskOptionsDesign defaultButtonHover" id="wjContextTaskOptionsAddTag" onclick="wjShowContextAddTag()">Neue Aufgabe</div>
						<div class="wjContextTaskOptionsDesign defaultButtonHover" id="wjContextTaskOptionsAddTask" onclick="appendNewSingleTask()">Neue Arbeit</div>
					</div>
				</div>
			</div>
			<div class="wjContextSurrounderResponseDesign" id="wjContextSurrounderReflex">
				<div class="wjContextResponseTitleDesign" id="wjContextReflexTitle">Reflexion</div>
				<textarea class="wjContextTextareaDesign" id="wjContextTextareaReflex" name="wjContextTextareaReflex"></textarea>
			</div>
			<div class="wjContextSurrounderResponseDesign" id="wjContextSurrounderProblems">
				<div class="wjContextResponseTitleDesign" id="wjContextProblemsTitle">Probleme</div>
				<textarea class="wjContextTextareaDesign" id="wjContextTextareaProblems" name="wjContextTextareaProblems"></textarea>
			</div>
			<div class="wjContextSurrounderResponseDesign" id="wjContextSurrounderNexttime">
				<div class="wjContextResponseTitleDesign" id="wjContextNexttimeTitle">Weiteres Vorgehen</div>
				<textarea class="wjContextTextareaDesign" id="wjContextTextareaNexttime" name="wjContextTextareaNexttime"></textarea>
			</div>
			<div class="wjContextSurrounderExtensionsDesign" id="wjContextSurrounderExtensions">
				<input type="button" value="Statistik" class="wjContextExtensionButtonsDesign defaultButtonHover" id="wjContextExtensionButtonStatistic" name="wjContextExtensionButtonStatistic" onclick="getStatistic()"/>
				<input type="button" value="Exportieren" class="wjContextExtensionButtonsDesign defaultButtonHover" id="wjContextExtensionButtonExport" name="wjContextExtensionButtonExport" onclick="exportToPDF()"/>
				<input type="button" value="Reset" class="wjContextExtensionButtonsDesign defaultButtonHover" id="wjContextExtensionButtonReset" name="wjContextExtensionButtonReset" onclick="onResetClick()"/>
				<input type="submit" value="Speichern" class="wjContextExtensionButtonsDesign defaultButtonHover" id="wjContextExtensionButtonSave" name="wjContextExtensionButtonSave" onclick="saveNewJournal()"/>
			</div>
		</form>
	</div>
	<div class="wjBlackscreenInformationDesign" id="wjBlackscreenInformation" name="wjBlackscreenInformation">
		<input type="button" class="wjNewTaskQuestionerCancelDesign wjNewTaskQuestionerSaveDesign" id="wjNewTaskQuestionerCancel" value="X" onclick="cancelNewTaskTag(true)">
		<input type="text" class="wjNewTaskQuestionerDesign" id="wjNewTaskQuestioner" name="wjNewTaskQuestioner" placeholder="Name der Aufgabe"\>
		<input type="button" class="wjNewTaskQuestionerSaveDesign" id="wjNewTaskQuestionerSave" value="Save" onclick="addNewTaskTag()">
	</div>
	<div class="wjStatisticsSurrounderDesign wjBlackscreenInformationDesign" id="wjStatisticsSurrounder">
		<div class="wjStatisticsTitleDesign"></div>
		<table class="wjStatisticsTableDesign"></table>
		<input type="button" class="wjNewTaskQuestionerCancelDesign wjNewTaskQuestionerSaveDesign" id="wjNewTaskQuestionerCancelStatistic" value="X" onclick="cancelNewTaskTag(true)">
	</div>
</div>