<?php
	//Es muss das Configfile für das Ajax included werden
	require_once("../../config.ajax.php");
	
	//Variablen
	$rapID = $_POST["rapID"];
	$option = $_POST["getOption"];
	
	//Überprüft die Berechtigungen
	if(handleUserdata()){
		switch($option){
			case "date":
				echo jlGetDateFromRapport($rapID, $db, true, "r_date");
				break;
			case "reflex":
				echo jlGetReflexFromRapport($rapID, $db, true, "r_reflexion");
				break;
			case "prob":
				echo jlGetProbFromRapport($rapID, $db, true, "r_problems");
				break;
			case "next":
				echo jlGetNextFromRapport($rapID, $db, true, "r_nexttext");
				break;
			default:
				echo false;
		}
	}
?>