<?php
	//Es muss das Configfile für das Ajax included werden
	require_once("../../config.ajax.php");
	
	// Überprüft die Berechtigungen
	if(handleUserdata()){
		//Initialisiert eine neues Arbeitsjournal
		$journal = new Journal();
		
		//Speichert odert updated das Arbeitsjournal
		$swHtmlCode = $journal->saveJournal($_POST, $db);
		
		echo $swHtmlCode;
	}
?>