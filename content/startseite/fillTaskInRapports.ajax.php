<?php
	//Es muss das Configfile für das Ajax included werden
	require_once("../../config.ajax.php");
	
	//Variablen
	$rapID = $_POST["rapID"];
	$counterDivs = 1;
	$newHtmlWorkTask = "";
	
	//Überprüft die Berechtigungen
	if(handleUserdata()){
		foreach(jlGetTaskFromID($rapID, $db) as $task){
			$newHtmlWorkTask .= 
				'<div class="wjContextTaskContentDesign" id="wjContextTaskContent'.$counterDivs.'">
					<div class="wjContextTaskDesign" id="wjContextTaskOne">
						<select class="wjContextTaskTagDesign wjContextTaskSelectorToFill" name="wjContextTaskSelector'.$counterDivs.'" id="wjContextTaskSelector'.$counterDivs.'"><option value="'.$task["t_taskname"].'" selected>'.$task["t_taskname"].'</option>'.__getAllTagsByRapportID(handleUserdata("userID"), $task["t_taskname"], $db).'</select>
					</div>
					<div class="wjContextTaskDesign" id="wjContextTaskTwo">
						<input type="time" class="wjContextTaskTagDesign" name="wjContextTaskShould'.$counterDivs.'" id="wjContextTaskShould'.$counterDivs.'" value="'.$task["t_shouldtime"].'" required=required>
					</div>
					<div class="wjContextTaskDesign" id="wjContextTaskThree">
						<input type="time" class="wjContextTaskTagDesign" name="wjContextTaskIs'.$counterDivs.'" id="wjContextTaskIs'.$counterDivs.'" value="'.$task["t_istime"].'" required=required>
					</div>
					<div class="wjContextTaskDesign" id="wjContextTaskFour">
						<textarea class="wjContextTaskTagDesign" name="wjContextTaskComment'.$counterDivs.'" id="wjContextTaskComment'.$counterDivs.'">'.$task["t_description"].'</textarea>
					</div>
				</div>';
			$counterDivs++;
		}
		echo $newHtmlWorkTask;
	}
	
	function __getAllTagsByRapportID($userID, $taskToExclude, $db){
		$inputOptions = "";
		foreach(jlGetEmptyTasks($userID, $taskToExclude, $db) as $excludedTask){
			$excludedTask = $excludedTask["sw_work"];
			$inputOptions .= "<option value='$excludedTask'>$excludedTask</option>";
		}
		return $inputOptions;
	}
?>