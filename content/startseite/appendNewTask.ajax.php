<?php
	//Es muss das Configfile für das Ajax included werden
	require_once("../../config.ajax.php");
	
	//Variablen
	$wjCountST = $_POST["wjCountST"];
	
	//Überprüft die Berechtigungen
	if(handleUserdata()){
		//Initialisiert eine neues Arbeitsjournal
		$journal = new Journal();
		
		//Fügt ein neues Arbeitsjournal hinzu
		$swHtmlCode = $journal->getNewSingleWorkTask($wjCountST);
		
		echo $swHtmlCode;
	}
?>