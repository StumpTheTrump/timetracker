<?php
	//Es muss das Configfile für das Ajax included werden
	require_once("../../config.ajax.php");

	//Überprüft die Berechtigungen
	if(handleUserdata()){
		//Prüft ob es diese Bezeichnung bereits gibt
		$getFillOptions = wjGetTaskFromUser(handleUserdata("userID"), $db);
		if($getFillOptions != null){
			$codeToReturn = "";
			foreach($getFillOptions as $options){
				$tagname = $options["sw_work"];
				$codeToReturn .= '<option value="'.$tagname.'">'.$tagname.'</option>';
			}
			echo $codeToReturn;
		}else{
			echo "";
		}
	}
?>