<?php
	//Es muss das Configfile für das Ajax included werden
	require_once("../../config.ajax.php");
	
	//Variablen
	$sMonth = strtolower ($_POST["sMonth"]);
	
	//Überprüft die Berechtigungen
	if(handleUserdata()){
		switch($sMonth){
			case "januar":
				$sMonth = "%-1-%";
				break;
			case "februar":
				$sMonth = "%-2-%";
				break;
			case "märz":
				$sMonth = "%-3-%";
				break;
			case "april":
				$sMonth = "%-4-%";
				break;
			case "mai":
				$sMonth = "%-5-%";
				break;
			case "juni":
				$sMonth = "%-6-%";
				break;
			case "juli":
				$sMonth = "%-7-%";
				break;
			case "august":
				$sMonth = "%-8-%";
				break;
			case "september":
				$sMonth = "%-9-%";
				break;
			case "oktober":
				$sMonth = "%-10-%";
				break;
			case "november":
				$sMonth = "%-11-%";
				break;
			case "dezember":
				$sMonth = "%-12-%";
				break;
			default:
				$sMonth = "";
				break;
		}
		
		if($sMonth != ""){
			$counterDivs = 0;
			foreach(jlDGetAllPreviewJournals($sMonth, handleUserdata("userID"), $db) as $preview){
				$counterDivs++;
				$r_ID = $preview["r_id"];
				$r_Date = explode("-", $preview["r_date"]);
				$formatedDate = $r_Date[2] . "." . $r_Date[1] . "." . $r_Date[0];
				$newDate = '<div class="wjOptionsJournalDesign" id="wjOptionsJournal'.$counterDivs.'" onclick="fillInRapports('."'".$r_ID."'".', '."'".$counterDivs."'".')">'.$formatedDate.'</div>';
				echo $newDate;
			}
		}
	}
?>