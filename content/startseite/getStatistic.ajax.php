<?php
	//Es muss das Configfile für das Ajax included werden
	require_once("../../config.ajax.php");
	
	//Variablen
	$date = $_POST["date"];
	$rID = null;
	if($date != ""){
		$rID = jlCheckDateFromUser($date, handleUserdata("userID"), $db, true, "r_id");
	}
	$codeOutput = "
					<tr>
						<th class='tableStatisticTitle'>Arbeit</th>
						<th class='tableStatisticTitle'>Ist-/</th>
						<th class='tableStatisticTitle'>Soll-/</th>
						<th class='tableStatisticTitle'>Status</th>
					</tr>
				";

	//Überprüft die Berechtigungen
	if(handleUserdata()){
		if($rID != null){
			foreach(jlGetTaskFromID($rID, $db) as $task){
				$name = $task["t_taskname"];
				$shouldTime = $task["t_shouldtime"];
				$isTime = $task["t_istime"];
				$outputMSG = "";
				if($shouldTime > $isTime){
					$endTime = ((strtotime($shouldTime) - strtotime($isTime))/60);
					$outputMSG .= "'$endTime'  Min. noch übrig";
				}else{
					$endTime = ((strtotime($isTime) - strtotime($shouldTime))/60);
					$outputMSG .= "'$endTime' Min. zulange benötigt";
				}
				$codeOutput .= 
					"
						<tr class='statisticTableTR'>
							<td>".$name."</td>
							<td>".$isTime."</td>
							<td>".$shouldTime."</td>
							<td>".$outputMSG."</td>
						</tr>
					";
			}
			echo $codeOutput;
		}else{
			echo "<b>Unter diesem Datum wurden noch keine Tasks gespeichert!</b>";
		}
	}
?>