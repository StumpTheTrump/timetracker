<?php
	//Es muss das Configfile für das Ajax included werden
	require_once("../../config.ajax.php");
	
	//Variablen
	$wjNewTag = $_POST["wjNewTag"];
	
	//Überprüft die Berechtigungen
	if(handleUserdata()){
		if($wjNewTag != ""){
			//Prüft ob es diese Bezeichnung bereits gibt
			if(wjCheckNewTag(handleUserdata("userID"), $wjNewTag, $db) == null){
				//Fügt eine neue Bezeichnung hinzu
				wjAddNewTag($wjNewTag, handleUserdata("userID"), $db);
				echo "$wjNewTag wurde erfolgreich hinzugefügt";
				echo "<script>$('.wjContextTaskSelectorToFill').append(".'"'."<option value='$wjNewTag'>$wjNewTag</option>".'"'.");</script>";
			}else{
				echo "Es gibt bereits einen Arbeitstag mit dem Titel '$wjNewTag'";
			}
		}else{
			echo secureOutput("Das Feld darf nicht leer sein!");
		}
	}
?>