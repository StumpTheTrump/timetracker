<?php
//Es muss das Configfile für das Ajax included werden
require_once("../../config.ajax.php");

//Variablen
$name = $_POST["username"];
$pword = $_POST["password"];
$selectUsername = "";
$selectPassword = "";
$selectUserID = "";

//Funktion zum entschlüsseln des Passwortes
function encrypt_password($password) {
	$secret_salt = "setasalt";
	$salted_password = $secret_salt . $password;
	$password_safe = hash('sha256', $salted_password);

	return $password_safe;
}

$pword_safe = encrypt_password($pword);
$selectUsername = logGetUsernameStmnt($name, $db);

//Prüft auf den Benutzernamen
if($selectUsername){
	$selectUsername = logGetUsernameStmnt($name, $db, true, "email");
}

//Wenn alles valid ist, speichert es die Daten in ein Benutzerobjekt
//Auf dieses Benutzerobjekt kann man später von überall her zugreiffen (sehr praktisch)
if($selectUsername == $name){
	$selectPassword = logGetPasswordStmnt($selectUsername, $db);
	if($selectPassword){
		$selectPassword = logGetPasswordStmnt($selectUsername, $db, true, "password");
	}
	
	if($selectPassword == $pword_safe){
		logGetUserID($name, $db, true, "userID");
		
		//Objekt 'User' in Session
		session_start();
		$_SESSION["userdata"] = serialize(new User($selectUsername, $db));
		echo "<script>isLogedIn = true</script>";
	}else{
		echo secureOutput("Benutzername oder Passwort nicht korrekt");
	}
}else{
	echo secureOutput("Benutzername oder Passwort nicht korrekt");
}

?>