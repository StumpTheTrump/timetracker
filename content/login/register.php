<?php
//mit DB verbinden
require_once("../../config.ajax.php");

//Variablen
$email = strtolower($_POST["username"]);
$pword = $_POST["password"];
$pwordAgain = $_POST["passwordAgain"];
$selectUsername = "";
$isRegistrValid = false;

//Prüft nicht nur auf dem Client, sondern auch auf dem Server nach der Korrektheit der Daten
if(preg_match('/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/', $email)){
	if(preg_match('/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).{4,}$/', $pword)){
		if(preg_match('/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).{4,}$/', $pwordAgain)){
			$isRegistrValid = true;
		}else{
			echo secureOutput("Es muss ein gültiges Passwort angegeben werden!");
		}
	}else{
		echo secureOutput("Es muss ein gültiges Passwort angegeben werden!");
	}
}else{
	echo secureOutput("Es muss eine gültige Email angegeben werden!");
}

//Falls alles valid ist, speichert es die Daten in die DB
if($isRegistrValid){
	if($pword == $pwordAgain){
		//Passwort verschlüsseln
		$secret_salt = "setasalt";
		$salted_password = $secret_salt . $pword;
		$password_safe = hash('sha256', $salted_password);
		
		$selectUsername = regGetUsernameStmnt($email, $db);
		if($selectUsername){
			$selectUsername = regGetUsernameStmnt($email, $db, true, "email");
		}
		
		if($selectUsername != $email){
			$photoPath = "./img/userpics/default.png";
			//Registration durch 'statements.php'
			regRegistrInStmnt($email, $password_safe, $photoPath, $db);
			echo secureOutput("Du wurdest erfolgreich registriert!");
			echo "<script>isRegistred = true";
		}else{
			echo secureOutput("Dieser Benutzername ist bereits vergeben");
		}
	}else{
		echo secureOutput("Das Passwort stimmt nicht überein");
	}
}
?>