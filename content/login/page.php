<!-- Content zur Registration und dem Login -->
<div class="surrounderOutput">
	<label class="outputMsg"></label>
</div>

<div class="loginLayout">
	<label class="loginTitle">Login</label></br></br></br>
	<form class="myLogin">
		<input type="text" id="usernameLog" class="styleField" name="username" placeholder="Benutzername"></br></br>
		<input type="password" id="passwordLog" class="styleField" name="password" placeholder="Passwort"></br></br>
		<input type="submit" value="Senden" id="loginButton" name="senden" onClick="submitLogin()">
	</form>
</div>

<div class="registerLayout">
	<label class="registerTitle">Registrieren</label></br></br></br>
	<form class="myRegister">
		<input type="text" id="usernameReg" class="styleField" name="username" placeholder="Benutzername"></br></br>
		<input type="password" id="passwordReg" class="styleField" name="password" placeholder="Passwort"></br></br>
		<input type="password" id="passwordAgainReg" class="styleField" name="passwordAgain" placeholder="Passwort wiederholen"></br></br>
		<input type="submit" value="Senden" id="loginButton" name="senden" onClick="submitRegister()">
	</form>
</div>

<div class="surrounderSwitcher">
	<label class="switcher" onClick="switchRegister()"><< Ich muss mich erst registrieren >></label>
</div>