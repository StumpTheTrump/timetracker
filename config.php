<?php

//----------------------------------------------------
// ENABLE ERRORS
ini_set('display_errors', 'true');
error_reporting(E_ALL);

//----------------------------------------------------
// ROOT
define("DIR_ROOT",		'./');
define("DIR_JS",		DIR_ROOT."classes/js/");
define("DIR_PHP",		DIR_ROOT."classes/php/");
define("DIR_SQL",		DIR_ROOT."classes/sql/");
define("DIR_CSS",		DIR_ROOT."css/");
define("DIR_CONTENT",	DIR_ROOT."content/");
define("DIR_IMG", 		DIR_ROOT."img");

//----------------------------------------------------
// FUNCTIONS
include_once(DIR_ROOT."functions.php");

//----------------------------------------------------
// DATABASE
define('DB_HOST', 		'localhost');
define('DB_NAME', 		'root');
define('DB_PW', 		'');
define('DB_DATABASE', 	'basicdatabase');

//----------------------------------------------------
// AUTOLOAD
spl_autoload_register(function($class){
	include_once(DIR_PHP . $class . '.php');
});